__author__ = 'kmcinti2'
import webbrowser


class Video():
    """"This class provides a way to basic video information."""

    def __init__(self, title, duration, description, video_sample):
        self.title = title
        self.duration = duration
        self.description = description
        self.video_sample = video_sample

    def show_video(self):
        webbrowser.open_new(self.video_sample)


class TvShow(Video):
    """"This class provides a way to store TV information. Inherits from the Video class."""

    def __init__(self, title, duration, season, episode_number, description, station, video_sample):
        Video.__init__(title, duration, description, video_sample)
        self.season = season
        self.episode_number = episode_number
        self.station = station


class Movie(Video):
    """This class provides a way to store movie information"""

    VALID_RATINGS = ['G', 'PG', 'PG-13', 'R']

    def __init__(self, title, duration, description, poster, video_sample):
        Video.__init__(self, title, duration, description, video_sample)
        self.title = title
        self.poster_image_url = poster