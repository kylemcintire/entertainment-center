__author__ = 'kmcinti2'

import media
#import fresh_tomatoes

toy_story = media.Movie('Toy Story',
                        '1 hour 30 min',
                        'A story of a boy and his toys that come to life',
                        'https://upload.wikimedia.org/wikipedia/en/1/13/Toy_Story.jpg',
                        'https://www.youtube.com/watch?v=KYz2wyBy3kc')

avengers = media.Movie('Avengers',
                       '3 hours',
                       'Marvel heroes team up to take on Loki',
                       'https://upload.wikimedia.org/wikipedia/en/f/f9/TheAvengers2012Poster.jpg',
                       'https://www.youtube.com/watch?v=eOrNdBpGMv8')

up = media.Movie('Up',
                 '2 hours',
                 'Disgruntled old man finds happiness after tragedy',
                 'https://upload.wikimedia.org/wikipedia/en/0/05/Up_%282009_film%29.jpg',
                 'https://www.youtube.com/watch?v=pkqzFUhGPJg')

movies = [toy_story, avengers, up]
#fresh_tomatoes.open_movies_page(movies)
